

require('dotenv').config();
const express = require('express');
const app = express();
const User = require('./db/user');
const PendingUser = require('./db/pending-user');
const bodyParser = require('body-parser')
const { sendConfirmationEmail } = require('./mailer')

const cors = require('cors');
const corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}

app.use(bodyParser.json())
app.use(cors(corsOptions));

app.get('/api/test', (req, res) => {
  res.json({message: 'Hello World!'});
})

app.get('/api/activate/user/:hash', async (req, res) => {
  const { hash } = req.params;

  try {
    const user = await PendingUser.find({_id: hash})
    if(!user) {
      return res.status(422).send("user cannot be activated!")
    }

    const newUser = new User({...user.data})
    await newUser.save()
    await user.remove()
    res.json({message: `User ${hash} has been activated!`});
    return;
  } catch {
    return res.status(422).send("user cannot be activated! // catch")
  }

  res.json({message: `User ${hash} has been activated`});
  return;
})

app.delete('/api/user/delete', async (req, res) => {
  try {
    const user = await PendingUser.find({email});
    await user.remove();
    return res.json({message: 'User has been removed!'})
  } catch(e) {
    return res.status(422).send('Cannot delete user!');
  }
})

app.post('/api/register', async (req, res) => {
  const {firstName, lastName, username, email, mobileNo, password} = req.body
  try {
    const rUser = await User.find({email});
    const pUser = await PendingUser.find({email});
    if (pUser || rUser) { return res.status(422).send('User email is already in the database.');}

    const newUser = new PendingUser({firstName, lastName, username, email, mobileNo, password});
    await newUser.hashPassword();
    await newUser.save();
    await sendConfirmationEmail({toUser: newUser.data, hash: newUser.data._id})
    res.json({message: 'An activation link has been sent to your email address. Please click on it to activate your account and finish registration.'});
  } catch(e) {
    res.status(422).send(e.message);
  }
})

app.post('/api/login', async (req, res) => {
  const {email, password} = req.body
  try {
    const user = await User.find({email});
    if (!user) { return res.status(422).send('Incorrect email or password.')}

    const isValid = await user.validatePassword(password);
    if (!isValid) { return res.status(422).send('Incorrect password!')}

    
    res.json({message: 'Login successful.'});
  } catch(e) {
    res.status(404).send(e.message);
  }
  
})

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => console.log(`> Connected to ${PORT}`));
