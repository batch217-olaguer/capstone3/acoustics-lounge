import React from 'react';
import { Form, Row, Col, Button, Alert } from 'react-bootstrap';
// import { PageTitle } from '../components';
import { useForm } from "react-hook-form";
import { useMutate } from "restful-react";
import { useRouter } from 'next/router';
import Swal from "sweetalert2";

const Login = () => {
  const router = useRouter();
  const { mutate: login, loading, error } = useMutate({
    verb: 'POST',
    path: 'login'
  });
  const { register, handleSubmit } = useForm();

  const onSubmit = data => {
  //   Swal.fire({
  //     title: "Login Successful!",
  //     icon: "success",
  //     text: "Redirecting to homepage."
  // })
    login(data).then(_ => router.push('/'));
  }

  return (
    
     <div>
      <h1>Login</h1>
      <Row>
        <Col md={{ span: 6, offset: 3 }}>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                // ref={register}
                {...register('email', {required: true})}
                type="email"
                // name="email"
                required
                placeholder="Enter email" />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                // ref={register}
                {...register('password', {required: true})}
                // name="password"
                type="password"
                required
                placeholder="Password" />
            </Form.Group>
            { error &&
              <Alert variant="danger">
                {error?.data}
              </Alert>
            }
            <Button
              disabled={loading}
              variant="primary"
              type="submit">
              Submit
            </Button>
            </Form>
          </Col>
        </Row>
      </div>
    
    )
 }



export default Login;
