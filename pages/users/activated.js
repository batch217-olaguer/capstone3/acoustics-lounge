import React from 'react';
import { Alert } from 'react-bootstrap';
import Link from 'next/link';

const UserActivation = () => {
  return (
    <>
      <div>
        <Alert variant='success'>
            User account activated. Thank you. You can login now!{' '}
            <Link href="/login">
                Login
            </Link>
        </Alert>
      </div>
    </>
  )
}

export default UserActivation;