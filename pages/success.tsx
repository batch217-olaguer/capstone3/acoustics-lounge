import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import { BsBagCheckFill } from 'react-icons/bs'
// import { useRoute } from 'next/router'

import { useStateContext } from '../context/StateContext'
import {runFireworks} from '../lib/utils'

const Success = () => {
//    const [order, setOrder] = useState(null)
const { setCartItems, setTotalPrice, setTotalQuantities } = useStateContext()

useEffect(() => {
    localStorage.clear()
    setCartItems([])
    setTotalPrice(0)
    setTotalQuantities(0)
    runFireworks()
}, [])

  return (
    <div className="success-wrapper">
        <div className="success">
            <p className="icon">
                <BsBagCheckFill />
            </p>
            <h2>Thank you for purchasing!</h2>
            <p className="email-msg">Check your email inbox for the receipt.</p>
            <p className="description">
                If you have anyquestions, please email us at<a className="email" href="mailto:order_help@acousticslounge.com">order_help@acousticslounge.com</a>
            </p>
            <Link href="/">
                <button type="button" className="btn">
                    Continue Shopping
                </button>
            </Link>
        </div>
    </div>
  )
}

export default Success