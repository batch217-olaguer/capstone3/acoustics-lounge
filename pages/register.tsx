import React from 'react'
import { Form, Row, Col, Button, Alert } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useMutate } from 'restful-react';
import { useRouter } from 'next/router';
import Swal from "sweetalert2";


const Register = () => {
  const [fName, setFName] = useState('');
  const [lName, setLName] = useState('');
  const [uName, setUName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);

  // FOR USE EFFECT dependencies.
//   useEffect(() => {


//     if((fName === '' && lName ==='' && uName ==='' && email === '' && mobileNo === '' && password1 === '' && password2 ==='') ){
//         setIsActive(true);
//     }
//     else{
//         setIsActive(false);
//     }

// }, [fName, lName, email, mobileNo, password1, password2])

  const [info, setInfo] = useState();
  const { mutate: registerUser, loading, error } = useMutate({
    verb: 'POST',
    path: 'register'
  });
  const { register, handleSubmit } = useForm();

  // const onSubmit = data => {
  //     if(password1 !== password2){
  //       Swal.fire({
  //         title: "Verification password does not match entered password.",
  //         icon: "error",
  //         text: "Kindly re-enter verification password to continue the registration process."
  //     })
  //     } else {
  //       setInfo();
  //       registerUser(data)
  //         .then(_ => setInfo('Please visit your email address and active your account'));
  //     }
  // }

  const onSubmit = (data) => {
    if(password1 !== password2){
      Swal.fire({
        title: "Verification password does not match entered password.",
        icon: "error",
        text: "Kindly re-enter verification password to continue the registration process."
    })
    } else {
      // setInfo();
      registerUser(data)
        .then(_ => setInfo('An activation link has been sent to your email address. Please click on it to activate your account and finish registration.'));
    }
}

  return (
    <div className="bwm-form">
      <h1>Register</h1>
      <Row>
        <Col md={{ span: 6, offset: 3 }}>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Form.Group controlId="formFirstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                {...register('firstName', {required: true})}
                type="text"
                placeholder="First name"
                // value={fName}
                required
              />
            </Form.Group>
            <Form.Group controlId="formLastName">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                {...register('lastName', {required: true})}
                type="text"
                placeholder="Last name"
                // value={lName}
                required
              />
            </Form.Group>
            <Form.Group controlId="formUsername">
              <Form.Label>Username</Form.Label>
              <Form.Control
                {...register('username', {required: true})}
                type="text"
                placeholder="Username"
                // value={uName}
                required
              />
            </Form.Group>
            <Form.Group controlId="formEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                {...register('email', {required: true})}
                type="email"
                placeholder="Email"
                value={email}
                onChange={e => setEmail(e.target.value)}
                required
                /> 
              <Form.Text className="text-muted">
              </Form.Text>
            </Form.Group>
            <Form.Group controlId="formMobileNo">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                {...register('mobileNo', {required: false})}
                type="number"
                placeholder="09xxxxxxxxx  Optional"
                // value={mobileNo}
                /> 
              <Form.Text className="text-muted">
              </Form.Text>
            </Form.Group>
            <Form.Group controlId="formPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                {...register('password', {required: true})}
                type="password"
                placeholder="Password" 
                value={password1}
                onChange={e => setPassword1(e.target.value)}
                required
                />           
            </Form.Group>
            <Form.Group controlId="formPasswordVerify">
              <Form.Label>Verify Password</Form.Label>
              <Form.Control
                name="passwordVerification"
                type="password"
                placeholder="Re-enter your password" 
                value={password2}
                onChange={e => setPassword2(e.target.value)}
                required
                />           
            </Form.Group>
            { info &&
              <Alert variant="success">
                {info}
              </Alert>
            }
            { error &&
              <Alert variant="danger">
                {error?.data}
              </Alert>
            }
            {/* {
              isActive
                ?
                  <Button
                    disabled={loading}
                    variant="primary"
                    type="submit">
                    Submit
                  </Button>
                :
                  <Button
                    disabled
                    variant="primary"
                    type="submit">
                    Submit
                  </Button>
            } */}
            <Button
              disabled={loading}
              variant="primary"
              type="submit">
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </div>
  )
}



export default Register;
