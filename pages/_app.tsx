import React from 'react'
import { Toaster } from 'react-hot-toast'

import '../styles/globals.css'
import '../styles/index.scss'
import { Layout } from '../components'
import { StateContext} from '../context/StateContext'
import { RestfulProvider } from 'restful-react'
// import App from 'next/app'

// function MyApp({Component, pageProps}) {
//   return(
//     <StateContext>
//       <Layout>
//         <Toaster/>
//            <Component {...pageProps} />
//       </Layout>
//     </StateContext>
//   )
// }

// export default MyApp

const App = ({Component, pageProps}) => {
  // const isHome = Component.name === 'Home';
  // const Wrapper = Component.name === 'PortfolioDetail' ? React.Fragment : Container;
  return(
        <StateContext>
          <Layout>
            <Toaster/>
               <Component {...pageProps} />
          </Layout>
        </StateContext>
      )
}

export default ({...props}) =>
  <RestfulProvider base="https://acoustics-lounge-api.vercel.app/api">
    <App {...props}/>
  </RestfulProvider>;